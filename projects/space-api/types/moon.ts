export interface Moon {
  id: number;
  planet: string;
  name: string;
  description: string;
  imageUrl: string;
  wikiLink: string;
}
