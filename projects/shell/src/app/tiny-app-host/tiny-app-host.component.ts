import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { loadRemoteModule } from '@angular-architects/native-federation';

@Component({
  template: `<tiny-app></tiny-app>`,
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  standalone: true,
})
export default class TinyAppHostComponent {
  constructor() {
    loadRemoteModule('tinyApp', './App');
  }
}
