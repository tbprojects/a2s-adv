import { Routes } from '@angular/router';
import { loadRemoteModule } from '@angular-architects/native-federation';

export const routes: Routes = [
  {
    path: 'welcome',
    loadComponent: () => import('./welcome/welcome.component'),
  },
  {
    path: 'tiny-app',
    loadComponent: () => import('./tiny-app-host/tiny-app-host.component'),
  },
  {
    path: 'launches',
    loadChildren: () => loadRemoteModule('spaceApp', './LaunchesRoutes')
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'welcome'
  }
];
