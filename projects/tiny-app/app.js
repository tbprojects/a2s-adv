export default class TinyApp extends HTMLElement {
  connectedCallback() {
    console.log('Tiny App started');
    this.#renderContent();
  }

  disconnectedCallback() {
    console.log('Tiny App removed');
  }

  #renderContent() {
    this.style.display = 'block';
    this.style.textAlign = 'center';

    const logo = document.createElement('img');
    logo.alt = 'Tiny App logo';
    logo.src = `http://localhost:4560/app-logo.png`;
    this.appendChild(logo);

    const message = document.createElement('p');
    message.textContent = 'Tiny App works!';
    this.appendChild(message);
  }
}

customElements.define('tiny-app', TinyApp);
