import express from 'express';
import cors from 'cors';

const app = express();
const port = 4560;

app.use(cors())

app.use(express.static('projects/tiny-app'));

app.get('*', (req, res) => {
  res.sendFile('index.html', { root: 'projects/tiny-app' });
});

app.listen(port, () => {
  console.log('Server is running at http://localhost:4560...');
});
