import { booleanAttribute, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'lib-logo',
  imports: [],
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css']
})
export class LogoComponent {
  /** Should logo animate on hover? */
  @Input({transform: booleanAttribute}) animated: boolean = false;

  /** Percent level of grayscale */
  @Input() grayscale: number = 0;

  /** Optional click handler */
  @Output() clicked = new EventEmitter<Event>();
}
