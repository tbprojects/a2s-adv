import type { Meta, StoryObj } from '@storybook/angular';
import { fn } from '@storybook/test';
import { LogoComponent } from './logo.component';

const meta: Meta<LogoComponent> = {
  title: 'SharedLib/Logo',
  component: LogoComponent,
  tags: ['autodocs'],
  argTypes: {
    animated: {
      control: 'boolean',
    },
    grayscale: {
      control: { type: 'range', min: 0, max: 100, step: 1},
    }
  },
  args: {
    clicked: fn()
  },
};

export default meta;
type Story = StoryObj<LogoComponent>;

export const Default: Story = {
  args: {
  },
};

export const Animated: Story = {
  args: {
    animated: true,
  }
}

export const Gray: Story = {
  args: {
    grayscale: 100
  }
}
