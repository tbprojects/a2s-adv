import { InjectionToken } from '@angular/core';

export const BUTTON_DEFAULT_COLOR = new InjectionToken<string>('BUTTON_DEFAULT_COLOR');
