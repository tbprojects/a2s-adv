import { applicationConfig, Meta, StoryObj } from '@storybook/angular';
import { fn } from '@storybook/test';

import { ButtonComponent } from './button.component';
import { BUTTON_DEFAULT_COLOR } from './button.tokens';

const meta: Meta<ButtonComponent> = {
  title: 'Example/Button',
  tags: ['autodocs'],
  component: ButtonComponent,
  decorators: [
    applicationConfig({
      providers: [{
        provide: BUTTON_DEFAULT_COLOR,
        useValue: 'lightgray'
      }]
    })
  ],
  argTypes: {
    backgroundColor: {
      control: 'select',
      options: ['lightgray', 'red', 'green'],
    },
  },
  args: {
    onClick: () => fn()
  },
};

export default meta;
type Story = StoryObj<ButtonComponent>;

export const Default: Story = {
  args: {
    label: 'Default Button',
  },
};

export const Red: Story = {
  args: {
    label: 'Red Button',
    textColor: 'white',
    backgroundColor: 'red'
  },
};
