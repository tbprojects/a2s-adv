import { Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { BUTTON_DEFAULT_COLOR } from './button.tokens';

/**
 * This is **an important description** of the component
 * */
@Component({
  selector: 'app-button',
  standalone: true,
  styles: `button {
    border: none;
    padding: 4px 10px
  }`,
  template: `
    <button
      type="button"
      (click)="onClick.emit($event)"
      [style.color]="textColor"
      [style.background-color]="backgroundColor"
    >
      {{ label }}
    </button>`,
})
export class ButtonComponent {
  /** Required text to be displayed in button*/
  @Input({required: true})
  label!: string;

  /** Optional text color to use */
  @Input()
  textColor = 'black';

  /** Optional background color to use */
  @Input()
  backgroundColor = inject(BUTTON_DEFAULT_COLOR);

  /** Optional click handler */
  @Output()
  onClick = new EventEmitter<Event>();
}
