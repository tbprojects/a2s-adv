import { Component, computed, inject, input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-browser',
  templateUrl: './browser.component.html',
  styleUrls: ['./browser.component.css'],
  imports: [RouterLink]
})
export default class BrowserComponent {
  private sanitizer = inject(DomSanitizer);

  url = input('');
  safeUrl = computed(() => this.sanitizer.bypassSecurityTrustResourceUrl(this.url()));
}
