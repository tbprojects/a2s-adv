import { Component, inject } from '@angular/core';
import { LogoComponent } from 'shared-lib';
import { NasaViewerComponent } from '../../components/mars-viewer/nasa-viewer.component';
import { Store } from '@ngrx/store';
import { hideNavigation, showNavigation } from '../../actions/app.actions';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
  imports: [LogoComponent, NasaViewerComponent]
})
export default class StartComponent {
  private store = inject(Store)

  showNavigation(): void {
    this.store.dispatch(showNavigation());
  }

  hideNavigation(): void {
    this.store.dispatch(hideNavigation());
  }
}
