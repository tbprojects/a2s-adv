import { Component, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { A2sCommComponent } from 'a2s-comm';
import { APP_CONFIG } from './app-config-token';
import { IfFeatureEnabledDirective } from './modules/shared/if-feature-enabled/if-feature-enabled.directive';
import { isBusy } from './services/busy.interceptor';
import { UpdateService } from './services/update.service';
import { PushService } from './services/push.service';
import { Store } from '@ngrx/store';
import { selectNavigationVisible } from './selectors/app.selectors';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    imports: [RouterLink, RouterOutlet, A2sCommComponent, RouterLinkActive, IfFeatureEnabledDirective]
})
export class AppComponent {
  title = 'space-app';
  appVersion = inject(APP_CONFIG).version;
  update = inject(UpdateService)
  push = inject(PushService)
  store = inject(Store);
  busy = toSignal(isBusy);
  updateAvailable = toSignal(this.update.updateAvailable);
  unrecoverableError = toSignal(this.update.unrecoverableError);
  isSubscribed = toSignal(this.push.isSubscribed);
  navigationVisible = this.store.selectSignal(selectNavigationVisible);

  activateUpdate(): void {
    this.update.activateUpdate();
  }

  reloadApp(): void {
    this.update.reloadApp();
  }

  requestSubscription() {
    this.push.requestSubscription()
      .subscribe(() => alert('Subscribed!'));
  }

  revokeSubscription() {
    this.push.revokeSubscription()
      .subscribe(() => alert('Unsubscribed!'));
  }
}
