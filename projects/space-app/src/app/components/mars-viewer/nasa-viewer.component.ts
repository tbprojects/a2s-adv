import { Component, computed, linkedSignal, signal } from '@angular/core';

type ImageType = 'planets' | 'telescopes';

@Component({
  selector: 'app-nasa-viewer',
  imports: [],
  templateUrl: './nasa-viewer.component.html',
  styleUrls: ['./nasa-viewer.component.scss']
})
export class NasaViewerComponent {
  private images: Record<ImageType, string[]> = {
    planets: ['Earth', 'Mars', 'Mercury', 'Venus'],
    telescopes: ['Hubble', 'Kepler', 'NuSTAR', 'Spitzer', 'TESS', 'Webb']
  }

  availableTypes = signal(Object.keys(this.images) as ImageType[]);

  availableImages = computed(() => this.images[this.selectedType()]);

  selectedType = linkedSignal(() => this.availableTypes()[0]);

  selectedImage = linkedSignal(() => this.availableImages()[0]);

  selectType(type: ImageType) {
    this.selectedType.set(type);
  }

  selectImage(image: string) {
    this.selectedImage.set(image);
  }
}
