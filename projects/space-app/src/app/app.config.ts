import { ApplicationConfig, inject, provideAppInitializer, provideZoneChangeDetection, isDevMode } from '@angular/core';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { routes } from './app.routes';
import { provideAppImageLoader } from './app-image-loader';
import { AppConfigService } from 'space-api/services';
import { busyInterceptor } from './services/busy.interceptor';
import { provideServiceWorker } from '@angular/service-worker';
import { provideStore } from '@ngrx/store';
import * as fromApp from './reducers/app.reducer';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { provideEffects } from '@ngrx/effects';

function appConfigInitializer() {
  return inject(AppConfigService).getAppConfig();
}

export const appConfig: ApplicationConfig = {
  providers: [
    provideZoneChangeDetection({ eventCoalescing: true }),
    provideRouter(routes, withComponentInputBinding()),
    provideHttpClient(withInterceptors([busyInterceptor])),
    provideAppImageLoader(),
    provideAppInitializer(appConfigInitializer),
    provideServiceWorker('ngsw-worker.js', {
        enabled: !isDevMode(),
        registrationStrategy: 'registerWhenStable:30000'
    }),
    provideStore({ [fromApp.appFeatureKey]: fromApp.reducer }),
    provideStoreDevtools({ maxAge: 25, logOnly: !isDevMode() }),
    provideEffects(),
  ]
};
