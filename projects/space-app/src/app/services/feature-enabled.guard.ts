import { inject } from '@angular/core';
import { Router, CanMatchFn } from '@angular/router';
import { AppConfig } from 'space-api/types';
import { APP_CONFIG } from '../app-config-token';

export function featureEnabledGuard(featureName: keyof AppConfig['features']): CanMatchFn {
  return (route, segments) => {
    return inject(APP_CONFIG).features[featureName]  || inject(Router).createUrlTree(['forbidden']);
  }
}
