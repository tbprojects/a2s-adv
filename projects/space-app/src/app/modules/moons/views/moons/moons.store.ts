import { Moon } from 'space-api/types';
import { patchState, signalStore, withComputed, withHooks, withMethods, withState } from '@ngrx/signals';
import { computed, inject } from '@angular/core';
import { MoonsService } from 'space-api/services';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { pipe, switchMap } from 'rxjs';
import { tapResponse } from '@ngrx/operators';

type MoonsState = {
  moons: Moon[];
  selectedPlanet: string;
  filter: {
    query: string;
    order: 'asc' | 'desc';
  }
}

const initialState: MoonsState = {
  moons: [],
  selectedPlanet: '',
  filter: {
    query: '',
    order: 'asc'
  }
};

export const MoonsStore = signalStore(
  withState(initialState),

  withComputed(({moons, selectedPlanet, filter}) => {
    const filteredMoons = computed(() =>
      moons()
        .filter(({planet, name}) =>
          (!filter.query() || new RegExp(filter.query(), 'i').test(name)) &&
          (!selectedPlanet() || planet === selectedPlanet())
        )
        .sort(({name: a}, {name: b}) => filter.order() === 'asc' ? a.localeCompare(b) : b.localeCompare(a))
    );

    const filteredMoonsCount = computed(() => filteredMoons().length);

    const planets = computed(() => [...new Set(moons().map(({planet}) => planet))]);

    return { filteredMoons, filteredMoonsCount, planets };
  }),

  withMethods((store) => {
    const moonService = inject(MoonsService);

    const loadMoons = rxMethod<void>(
      pipe(
        switchMap(() =>
          moonService.getMoons().pipe(
            tapResponse({
              next: (moons) => patchState(store, {moons}),
              error: (e) => console.error(e),
            })
          )
        )
      )
    );

    const selectPlanet = (planet: string) => patchState(store, {selectedPlanet: planet});

    const filterQuery = (query: string) => patchState(store, (state) => ({filter: {...state.filter, query}}));

    const filterOrder = (order: 'asc' | 'desc') => patchState(store, (state) => ({filter: {...state.filter, order}}));

    return {loadMoons, selectPlanet, filterQuery, filterOrder};
  }),

  withHooks((store) => {
    const onInit = () => store.loadMoons()

    return { onInit };
  }),
);
