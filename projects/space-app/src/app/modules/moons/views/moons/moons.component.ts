import { Component, inject } from '@angular/core';
import { MoonsStore } from './moons.store';
import { NgOptimizedImage } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-moons',
  templateUrl: './moons.component.html',
  styleUrl: './moons.component.css',
  standalone: true,
  imports: [NgOptimizedImage, RouterLink],
  providers: [MoonsStore]
})
export default class MoonsComponent {
  store = inject(MoonsStore);
}
