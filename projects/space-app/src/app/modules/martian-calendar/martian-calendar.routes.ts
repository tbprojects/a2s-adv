import { Routes } from '@angular/router';
import { MartianCalendarComponent } from './martian-calendar.component';

const routes: Routes = [{ path: '', component: MartianCalendarComponent }];

export default routes;
