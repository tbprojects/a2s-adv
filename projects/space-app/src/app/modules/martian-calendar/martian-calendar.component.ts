import { Component, computed, inject } from '@angular/core';
// @ts-ignore
import DarianSystem from 'darian-system';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
    selector: 'app-martian-calendar',
    templateUrl: './martian-calendar.component.html',
    styleUrls: ['./martian-calendar.component.css'],
    imports: [ReactiveFormsModule],
})
export class MartianCalendarComponent {
  currentDate = new DarianSystem.Darian_Date().getDate();

  form = inject(FormBuilder).group({
    year: new Date().getFullYear(),
    month: new Date().getMonth() + 1,
    date: new Date().getDate()
  });

  formValue = toSignal(this.form.valueChanges, {initialValue: this.form.value});

  marsDate = computed(() => {
    try {
      const { year, month, date } = this.formValue();
      const hour = new Date().getHours();
      const minutes = new Date().getMinutes();
      const seconds = new Date().getSeconds();
      return new DarianSystem.Darian_Date(year, month, date, hour, minutes, seconds).getDate();
    } catch (e) {
      return 'invalid date';
    }
  });
}
