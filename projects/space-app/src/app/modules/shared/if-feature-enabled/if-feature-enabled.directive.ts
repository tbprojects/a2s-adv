import { computed, Directive, effect, inject, input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AppConfig } from 'space-api/types';
import { APP_CONFIG } from '../../../app-config-token';

@Directive({
  selector: '[appIfFeatureEnabled]',
})
export class IfFeatureEnabledDirective {
  private templateRef = inject(TemplateRef<void>);
  private viewContainer = inject(ViewContainerRef);
  private appConfig = inject(APP_CONFIG);

  featureName = input.required<keyof AppConfig['features']>({alias: 'appIfFeatureEnabled'});

  canAccessFeature = computed(() => this.appConfig.features[this.featureName()]);

  constructor() {
    effect(() => {
      if (this.canAccessFeature() && this.viewContainer.length === 0) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    })
  }
}
