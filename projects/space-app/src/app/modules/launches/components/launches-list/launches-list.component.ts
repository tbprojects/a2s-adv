import { Component, input, output, ChangeDetectionStrategy } from '@angular/core';
import { Launch, LaunchDetailsUpdate } from 'space-api/types';
import { LaunchesListItemComponent } from '../launches-list-item/launches-list-item.component';
import { LaunchDetailsComponent } from '../launch-details/launch-details.component';
import { ScrollingModule } from '@angular/cdk/scrolling';

@Component({
  selector: 'app-launches-list',
  templateUrl: './launches-list.component.html',
  styleUrls: ['./launches-list.component.css'],
  imports: [ScrollingModule, LaunchesListItemComponent, LaunchDetailsComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LaunchesListComponent {
  launchDetailsUpdate = output<LaunchDetailsUpdate>();
  launches = input.required<Launch[]>();

  updateDetails(launch: Launch, $event: string): void {
    this.launchDetailsUpdate.emit({id: launch.id, details: $event});
  }
}
