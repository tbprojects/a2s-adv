import { ChangeDetectionStrategy, Component, effect, input, output } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Launch, LaunchesQueryParams } from 'space-api/types';

@Component({
  selector: 'app-launches-search',
  templateUrl: './launches-search.component.html',
  styleUrls: ['./launches-search.component.css'],
  imports: [ReactiveFormsModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LaunchesSearchComponent {
  paramsChange = output<LaunchesQueryParams>();
  params = input.required<LaunchesQueryParams>();

  form = new FormGroup({
    sort: new FormControl<keyof Launch>('id', {nonNullable: true}),
    order: new FormControl<'ASC' | 'DESC'>('ASC', {nonNullable: true}),
    query: new FormControl<string>('', {nonNullable: true}),
  });

  constructor() {
    effect(() => this.form.patchValue(this.params()));
  }

  search(): void {
    this.paramsChange.emit(this.form.getRawValue());
  }
}
