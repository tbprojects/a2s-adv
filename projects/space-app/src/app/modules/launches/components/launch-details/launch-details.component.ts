import { ChangeDetectionStrategy, Component, input, output, signal } from '@angular/core';

@Component({
  selector: 'app-launch-details',
  templateUrl: './launch-details.component.html',
  styleUrls: ['./launch-details.component.css'],
  imports: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LaunchDetailsComponent {
  detailsUpdate = output<string>();
  details = input.required<string | null>();
  editorOpened = signal(false);

  formattedDetails(): string {
    return this.details()?.split('').map(char => char.toUpperCase()).join('') ?? '';
  }

  update(details: string): void {
    this.detailsUpdate.emit(details);
    this.closeEditor();
  }

  openEditor(): void {
    this.editorOpened.set(true);
  }

  closeEditor(): void {
    this.editorOpened.set(false);
  }
}
