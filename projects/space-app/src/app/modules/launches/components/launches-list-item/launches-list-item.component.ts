import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { Launch } from 'space-api/types';
import { DatePipe, NgOptimizedImage } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'tbody[appLaunchesListItem]',
  templateUrl: './launches-list-item.component.html',
  styleUrls: ['./launches-list-item.component.css'],
  imports: [DatePipe, NgOptimizedImage, RouterLink],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LaunchesListItemComponent {
  launch = input.required<Launch>();
}
