import { Component, inject } from '@angular/core';
import { ResourceWrapperComponent } from '../../../../components/resource-wrapper/resource-wrapper.component';
import { LaunchDetailsUpdate, LaunchesQueryParams } from 'space-api/types';
import { LaunchesStateService } from './launches-state.service';
import { LaunchesListComponent } from '../../components/launches-list/launches-list.component';
import { LaunchesSearchComponent } from '../../components/launches-search/launches-search.component';

@Component({
    selector: 'app-launches',
    templateUrl: './launches.component.html',
    styleUrls: ['./launches.component.scss'],
    imports: [ResourceWrapperComponent, LaunchesListComponent, LaunchesSearchComponent],
    providers: [LaunchesStateService]
})
export class LaunchesComponent {
  private launchesState = inject(LaunchesStateService)
  queryParams = this.launchesState.queryParams;
  launches = this.launchesState.launches;

  searchLaunches(params: LaunchesQueryParams): void {
    this.launchesState.searchLaunches(params);
  }

  updateLaunchDetails(detailsUpdate: LaunchDetailsUpdate): void {
    this.launchesState.updateLaunchDetails(detailsUpdate);
  }
}
