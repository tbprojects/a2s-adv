import { inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LaunchesService } from 'space-api/services';
import { LaunchDetailsUpdate, LaunchesQueryParams } from 'space-api/types';
import { rxResource, toSignal } from '@angular/core/rxjs-interop';

@Injectable()
export class LaunchesStateService {
  private launchesService = inject(LaunchesService);
  private router = inject(Router);
  private route = inject(ActivatedRoute);

  queryParams = toSignal(this.route.queryParams as Observable<LaunchesQueryParams>, {requireSync: true});

  launches = rxResource({
    request: () => ({queryParams: this.queryParams()}),
    loader: ({request}) => this.launchesService.getLaunches(request.queryParams),
  });

  searchLaunches(params: Partial<LaunchesQueryParams>): void {
    this.router.navigate(['.'], {queryParams: params, relativeTo: this.route});
  }

  updateLaunchDetails(detailsUpdate: LaunchDetailsUpdate): void {
    this.launchesService.updateDetails(detailsUpdate).subscribe(() => {
      this.launches.reload();
    });
  }
}
