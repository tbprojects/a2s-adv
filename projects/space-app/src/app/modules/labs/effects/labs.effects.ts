import { inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { catchError, concatMap, map, tap } from 'rxjs/operators';
import { filter, of } from 'rxjs';
import { LabsService } from 'space-api/services';
import {
  loadLabs,
  loadLabsFailure,
  loadLabsSuccess,
  removeLab,
  removeLabFailure,
  removeLabSuccess
} from '../actions/labs.actions';

@Injectable()
export class LabsEffects {
  private actions = inject(Actions);
  private labsService = inject(LabsService);

  loadLabs = createEffect(() =>
    this.actions.pipe(
      ofType(loadLabs),
      concatMap(() => this.labsService.getLabs().pipe(
        map(labs => loadLabsSuccess({labs})),
        catchError(() => of(loadLabsFailure()))
      ))
    )
  );

  removeLab = createEffect(() =>
    this.actions.pipe(
      ofType(removeLab),
      concatMap(({id}) => this.labsService.removeLab(id).pipe(
        filter(() => confirm('Czy na pewno?')),
        map(labs => removeLabSuccess({id})),
        catchError(() => of(removeLabFailure()))
      ))
    )
  );

  labsActionFailure = createEffect(() =>
    this.actions.pipe(
      ofType(loadLabsFailure, removeLabFailure),
      tap(() => alert('Operacja związana z laboratoriami nie powiodła się!'))
    ), {dispatch: false}
  );
}
