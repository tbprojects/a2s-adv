import { Component, inject, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Lab } from 'space-api/types';
import { RouterLink } from '@angular/router';
import { LabsListComponent } from '../../components/labs-list/labs-list.component';
import { loadLabs, removeLab } from '../../actions/labs.actions';
import { selectLabsAll } from '../../selectors/labs.selectors';

@Component({
    selector: 'app-labs',
    templateUrl: './labs.component.html',
    styleUrls: ['./labs.component.css'],
  imports: [RouterLink, LabsListComponent]
})
export class LabsComponent implements OnInit {
  private store = inject(Store);
  labs = this.store.selectSignal(selectLabsAll);

  ngOnInit(): void {
    this.store.dispatch(loadLabs());
  }

  removeLab(lab: Lab): void {
    this.store.dispatch(removeLab({id: lab.id!}))
  }
}
