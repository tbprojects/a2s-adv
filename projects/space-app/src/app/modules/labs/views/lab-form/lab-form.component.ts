import { Component, computed, inject, input } from '@angular/core';
import { ActivatedRoute, Router, RouterLink, RouterLinkActive } from '@angular/router';
import { LabsService } from 'space-api/services';
import { EquipmentTypes, Lab } from 'space-api/types';
import { LabFormService } from '../../services/lab-form/lab-form.service';
import { LabForm } from '../../services/lab-form/lab-form.types';
import { ReactiveFormsModule } from '@angular/forms';
import { LabEquipmentsFormComponent } from '../../components/lab-equipments-form/lab-equipments-form.component';
import { LabDetailsFormComponent } from '../../components/lab-details-form/lab-details-form.component';

@Component({
    selector: 'app-lab-form',
    templateUrl: './lab-form.component.html',
    styleUrls: ['./lab-form.component.css'],
    imports: [RouterLink, RouterLinkActive, ReactiveFormsModule, LabEquipmentsFormComponent, LabDetailsFormComponent]
})
export class LabFormComponent {
  private route = inject(ActivatedRoute);
  private router = inject(Router);
  private labService = inject(LabsService);
  private formService = inject(LabFormService);

  step = input<keyof Lab>();
  lab = input.required<Lab>();
  form = computed(() => this.formService.buildForm(this.lab()));

  saveLab(): void {
    if (this.form().invalid) {
      alert('Popraw formularz!');
      return;
    }

    const lab = this.formService.formToLab(this.form());
    this.labService.saveLab(lab).subscribe({
      next: () => this.router.navigate(['../..'], {relativeTo: this.route}),
      error: () => alert('Zapis nie powiódł się!')
    });
  }

  addEquipment(type: EquipmentTypes): void {
    this.formService.addEquipment(this.form(), type);
  }

  removeEquipment(index: number): void {
    this.formService.removeEquipment(this.form(), index);
  }
}
