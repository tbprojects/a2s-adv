import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { EquipmentTypes } from 'space-api/types';

export type LabDetailsForm = FormGroup<{
  name: FormControl<string>;
  moonId: FormControl<number | null>;
  autonomous: FormControl<boolean>;
  enableTags: FormControl<boolean>;
}>

export type LabEquipmentForm = FormGroup<{
  [s: string]: FormControl<any>;
  name: FormControl<string>;
  tag: FormControl<string>;
  type: FormControl<EquipmentTypes>;
}>

export type LabForm = FormGroup<{
  id: FormControl<number | null>,
  details: LabDetailsForm
  equipments: FormArray<LabEquipmentForm>
}>
