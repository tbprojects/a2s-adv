import { Injectable } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { startWith } from 'rxjs';
import { Lab, LabDetails, Equipment, EquipmentTypes, EquipmentComputer, EquipmentDetector } from 'space-api/types';
import { LabDetailsForm, LabEquipmentForm, LabForm } from './lab-form.types';

@Injectable({providedIn: 'root'})
export class LabFormService {
  readonly specificControls = {
    [EquipmentTypes.Computer]: (equipment: EquipmentComputer) => ({
      producer: new FormControl(equipment.producer),
      os: new FormControl(equipment.os),
    }),
    [EquipmentTypes.Detector]: (equipment: EquipmentDetector) => ({
      precision: new FormControl(equipment.precision),
      productionYear: new FormControl(equipment.productionYear),
    }),
    // add new types here
  };

  formToLab(form: LabForm): Lab {
    return {equipments: [], ...form.value} as Lab;
  }

  addEquipment(form: LabForm, type: EquipmentTypes): void {
    const formArray = form.controls.equipments;
    const enableTags = form.controls.details.controls.enableTags.value;
    const formGroup = this.buildEquipment({type, name: '', tag: ''} as Equipment);
    this.setEquipmentValidators(formGroup, enableTags);
    formArray.push(formGroup);
  }

  removeEquipment(form: LabForm, index: number): void {
    form.controls.equipments.removeAt(index);
  }

  buildForm(lab: Lab): LabForm {
    const form = new FormGroup({
      id: new FormControl(lab.id),
      details: this.buildDetails(lab.details),
      equipments: this.buildEquipmentList(lab.equipments)
    });
    this.observeEnableTags(form);
    this.observeAutonomous(form);
    return form;
  }

  private buildDetails(details: LabDetails): LabDetailsForm {
    return new FormGroup({
      name: new FormControl(details.name, {validators: [Validators.required], nonNullable: true}),
      moonId: new FormControl(details.moonId, {validators: [Validators.required]}),
      autonomous: new FormControl(details.autonomous, {nonNullable: true}),
      enableTags: new FormControl(details.enableTags, {nonNullable: true})
    });
  }

  private buildEquipmentList(equipments: Equipment[]): FormArray<LabEquipmentForm> {
    return new FormArray(equipments.map(item => this.buildEquipment(item)));
  }

  private buildEquipment(equipment: Equipment): LabEquipmentForm {
    const specificControls = equipment.type ? this.specificControls[equipment.type](equipment as any) : {};
    return new FormGroup({
      name: new FormControl(equipment.name, {validators: [Validators.required], nonNullable: true}),
      tag: new FormControl(equipment.tag, {validators: [Validators.required], nonNullable: true}),
      type: new FormControl(equipment.type, {validators: [Validators.required], nonNullable: true}),
      ...specificControls
    });
  }

  private observeAutonomous(form: LabForm): void {
    const control = form.controls.details.controls.autonomous;
    control.valueChanges
      .pipe(startWith(control.value))
      .subscribe((autonomous: boolean) => {
        if (autonomous) {
          form.controls.equipments.disable();
        } else {
          form.controls.equipments.enable();
        }
      });
  }

  private observeEnableTags(form: LabForm): void {
    const control = form.controls.details.controls.enableTags;
    control.valueChanges
      .pipe(startWith(control.value))
      .subscribe((enableTags: boolean) => {
        form.controls.equipments.controls.forEach(formGroup => this.setEquipmentValidators(formGroup, enableTags));
      });
  }

  private setEquipmentValidators(equipmentFormGroup: LabEquipmentForm, enableTags: boolean): void {
    const tagControl = equipmentFormGroup.controls.tag;
    if (enableTags) {
      tagControl.addValidators([Validators.required]);
    } else {
      tagControl.removeValidators([Validators.required]);
    }
    tagControl.updateValueAndValidity();
  }
}
