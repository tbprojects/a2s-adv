import { Component, input } from '@angular/core';
import { LabEquipmentForm } from '../../services/lab-form/lab-form.types';
import { FormComponent } from '../form-component';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-detector-form',
  templateUrl: './detector-form.component.html',
  styleUrls: ['./detector-form.component.css'],
  imports: [ReactiveFormsModule]
})
export class DetectorFormComponent implements FormComponent {
  formGroup = input.required<LabEquipmentForm>();

}
