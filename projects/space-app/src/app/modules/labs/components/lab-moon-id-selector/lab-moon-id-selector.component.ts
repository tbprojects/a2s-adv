import { Component, computed, effect, inject } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { MoonsService } from 'space-api/services';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-lab-moon-id-selector',
  templateUrl: './lab-moon-id-selector.component.html',
  styleUrls: ['./lab-moon-id-selector.component.css'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: LabMoonIdSelectorComponent, multi: true}
  ],
  imports: [ReactiveFormsModule]
})
export class LabMoonIdSelectorComponent implements ControlValueAccessor {
  private moons = toSignal(inject(MoonsService).getMoons(), {initialValue: []});

  planetControl = new FormControl<string | null>(null);

  moonIdControl = new FormControl<number | null>(null);

  selectedMoonId = toSignal(this.moonIdControl.valueChanges, {initialValue: this.moonIdControl.value});

  selectedPlanet = toSignal(this.planetControl.valueChanges, {initialValue: this.planetControl.value});

  planetOptions = computed(() => Array.from(new Set(this.moons().map(({planet}) => planet))));

  moonOptions = computed(() => this.moons().filter(moon => moon.planet === this.selectedPlanet()));

  onChange = (value: number | null) => {};

  onTouch = () => {};

  writeValue(moonId: number | null): void {
    this.moonIdControl.setValue(moonId);
  }

  constructor() {
    effect(() => this.planetControl.setValue(
      this.moons().find(moon => moon.id === this.selectedMoonId())?.planet ?? null)
    );
  }

  registerOnChange(fn: (value: number | null) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    isDisabled ? this.disableControls() : this.enableControls();
  }

  private disableControls(): void {
    this.moonIdControl.disable();
    this.planetControl.disable();
  }

  private enableControls(): void {
    this.moonIdControl.enable();
    this.planetControl.enable();
  }
}
