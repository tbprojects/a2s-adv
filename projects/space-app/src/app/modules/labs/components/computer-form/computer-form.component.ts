import { Component, input } from '@angular/core';
import { LabEquipmentForm } from '../../services/lab-form/lab-form.types';
import { FormComponent } from '../form-component';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-computer-form',
  templateUrl: './computer-form.component.html',
  styleUrls: ['./computer-form.component.css'],
  imports: [ReactiveFormsModule]
})
export class ComputerFormComponent implements FormComponent {
  formGroup = input.required<LabEquipmentForm>();

}
