import { Component, effect, input, viewChild, ViewContainerRef } from '@angular/core';
import { LabEquipmentForm } from '../../services/lab-form/lab-form.types';
import { ReactiveFormsModule } from '@angular/forms';
import { formComponentMap } from '../form-component-map';

@Component({
    selector: 'app-equipment-form',
    templateUrl: './equipment-form.component.html',
    styleUrls: ['./equipment-form.component.css'],
    imports: [ReactiveFormsModule]
})
export class EquipmentFormComponent {
  formComponentContainer = viewChild('formComponentContainer', {read: ViewContainerRef});
  formGroup = input.required<LabEquipmentForm>();

  constructor() {
    effect(() => {
      const container = this.formComponentContainer();
      const equipmentType = this.formGroup().getRawValue().type;
      const formComponent = formComponentMap.get(equipmentType);
      if (container && formComponent) {
        const ref = container.createComponent(formComponent);
        ref.instance.formGroup = this.formGroup;
      }
    });
  }
}
