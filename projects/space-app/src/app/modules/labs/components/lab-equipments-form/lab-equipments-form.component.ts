import { Component, input, output } from '@angular/core';
import { FormArray, FormControl, ReactiveFormsModule } from '@angular/forms';
import { EquipmentTypes } from 'space-api/types';
import { LabEquipmentForm } from '../../services/lab-form/lab-form.types';
import { KeyValuePipe } from '@angular/common';
import { EquipmentFormComponent } from '../equipment-form/equipment-form.component';

@Component({
    selector: 'app-lab-equipments-form',
    templateUrl: './lab-equipments-form.component.html',
    styleUrls: ['./lab-equipments-form.component.css'],
    imports: [ReactiveFormsModule, EquipmentFormComponent, KeyValuePipe]
})
export class LabEquipmentsFormComponent {
  EquipmentTypes = EquipmentTypes;

  formArray = input.required<FormArray<LabEquipmentForm>>();
  equipmentAdd = output<EquipmentTypes>();
  equipmentRemove = output<number>();

  selectedEquipmentType = new FormControl(EquipmentTypes.Computer, {nonNullable: true});

  addEquipment(): void {
    this.equipmentAdd.emit(this.selectedEquipmentType.value);
  }

  removeEquipment(index: number): void {
    this.equipmentRemove.emit(index);
  }
}
