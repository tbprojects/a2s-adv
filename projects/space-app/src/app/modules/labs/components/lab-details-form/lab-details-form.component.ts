import { Component, input } from '@angular/core';
import { LabDetailsForm } from '../../services/lab-form/lab-form.types';
import { ReactiveFormsModule } from '@angular/forms';
import { LabMoonIdSelectorComponent } from '../lab-moon-id-selector/lab-moon-id-selector.component';

@Component({
    selector: 'app-lab-details-form',
    templateUrl: './lab-details-form.component.html',
    styleUrls: ['./lab-details-form.component.css'],
    imports: [ReactiveFormsModule, LabMoonIdSelectorComponent]
})
export class LabDetailsFormComponent {
  formGroup = input.required<LabDetailsForm>();

}
