import { InputSignal } from '@angular/core';
import { LabEquipmentForm } from '../services/lab-form/lab-form.types';

export interface FormComponent {
  formGroup: InputSignal<LabEquipmentForm>;
}
