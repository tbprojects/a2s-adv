import { publicVapidKey } from 'dev-api/push-config';

export const environment = {
  production: true,
  publicVapidKey: publicVapidKey
};
