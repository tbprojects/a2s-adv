describe('ManageLab', () => {
  it('should allow to create a lab', () => {
    const labName = `Acme-${Date.now()}`;

    // Navigate to form
    cy.visit('/labs');
    cy.contains('button', 'Dodaj').click();

    // Fill details
    cy.contains('label', 'Nazwa').siblings('input').type(labName);
    cy.contains('label', 'włącz tagowanie').click();
    cy.get('app-lab-moon-id-selector').within(() => {
      cy.get('select').first().select('Jowisz');
      cy.get('select').last().select('Io');
    });

    // Add equipment
    cy.contains('button', 'Wyposażenie').click();
    cy.get('app-lab-equipments-form').within(() => {
      cy.get('select').select('Computer');
      cy.get('button').click();
    });

    // Fill equipment
    cy.get('app-equipment-form').within(() => {
      cy.contains('label', 'Nazwa').siblings('input').type('Macbook');
      cy.contains('label', 'Tag').siblings('input').type('A2779');
      cy.contains('label', 'Producent').siblings('select').select('Apple');
      cy.contains('label', 'System operacyjny').siblings('select').select('Macos');
    });

    // Save form
    cy.contains('button', 'Zapisz').click();

    // Check created lab
    cy.contains('tbody', labName).within(() => {
      // Check lab name
      cy.get('td').eq(0).should('have.text', labName);

      // Check lab location
      cy.get('td').eq(1).should('have.text', 'Io (Jowisz)');

      // Check lab autonomous
      cy.get('td').eq(2).should('have.text', 'false');

      // Check lab tag requirement
      cy.get('td').eq(3).should('have.text', 'true');

      // Check number of equipments
      cy.get('td').eq(4).should('have.text', '1');
    })
  });
});
