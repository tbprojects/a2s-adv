import { createProxyMiddleware } from 'http-proxy-middleware';
import express from 'express';

const app = express();
const port = 4550;

app.use(express.static('dist/space-app/browser'));

app.use('/api', createProxyMiddleware({
  target: 'http://localhost:4510',
  pathRewrite: { '^/api': '' },
}));

app.get('*', (req, res) => {
  res.sendFile('index.html', { root: 'dist/space-app/browser' });
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}...`);
});
